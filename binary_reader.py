import numpy as np
import csv
from csv_tab import *
import os.path
import struct


def read_bin(filepath, filename, blue_data, red_data): 
    counter = 0
    with open("%s/%s" %(filepath, filename), "rb") as f:
        x = f.read(4)
        while x :
            if counter % 2 == 0:
                blue_data.append(x)
            elif counter % 2 == 1:
                red_data.append(x)
            counter += 1
            x = f.read(4)
    return blue_data, red_data

def parse_bin(filepath, filename, data):
    counter = 0
    f1 = os.path.join(filepath, filename)
    with open(f1, "rb") as f:
        x = f.read(8)
        while x:
            data.append(struct.unpack('>ii', x))
            x = f.read(8)
    return data
        #print len(x)

def parse_binary(filepath, filename, data_blue, data_red):
    counter = 0
    f1 = os.path.join(filepath, filename)
    with open(f1, "rb") as f:
        x = f.read(4)
        while x:
            if counter % 2 == 0:  
                data_blue.append(struct.unpack('>i', x))
            elif counter % 2 == 1:
                data_red.append(struct.unpack('>i', x))
            x = f.read(4)
            counter += 1
    return data_blue, data_red

def main():        
    filepath = "/media/TOSHIBA EXT/repos/fret-inference/test_data/monomers/"
    filename = "asyn0000.dat"        
    blue_data = []
    red_data = []
    data_blue = []
    data_red = []
    data = []
     
    blue, red = read_bin(filepath, filename, blue_data, red_data)
    print len(blue_data), len(red_data)

    #bd, rd = parse_binary(filepath, filename, data_blue, data_red)
    #print len(bd), len(rd)

    data_b_r = parse_bin(filepath, filename, data)
    bd = []
    rd = []
    for i in data_b_r:
        bd.append(i[0])
        rd.append(i[1])
        
    f1 = open("%s/%s" %(filepath, "data_asyn0000.csv"), "w")
    for m,n in zip(bd, rd):
        f1.write("%s\t%s\n" %(m,n))
    f1.close()
     
    d1 = []
    d2 = []   
    blue_list, red_list = csv_tab(filepath, "asyn0000.txt")
    for w, x, y, z in zip(bd, rd, blue_list, red_list):
        d1.append(w-y)
        d2.append(x-z)

    print "Mins = ", min(d1), min(d2)
    print "Max's = ", max(d1), max(d2)
           
if __name__ == "__main__":
    main()


#for i, j in zip(blue_data, red_data):
#    blue.append(ord(i))
#    red.append(ord(j))

#print blue, red
