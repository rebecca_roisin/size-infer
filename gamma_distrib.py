from scipy.special import gamma, gammaln
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

def gamma_poisson_mixture(p, k, r):
    # parameters p and k
    # value r eg pmf(r)
    pref = (gamma(r+k) / (gamma(k+1) * gamma(r))) 
    x =  pref * (p**k) * ((1-p)**r)
    return x

x = np.arange(0, 50, 1)

vals = np.arange(1, 10, 1)
for w in vals:
    y = stats.nbinom.pmf(x, w, 10, loc=0)
    print min(y), max(y)
    plt.scatter(x, y)
plt.show()