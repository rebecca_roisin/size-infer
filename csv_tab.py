import csv
import numpy as np

def csv_tab(filepath, filename):
    f = open("%s/%s" %(filepath, filename), "rb")
    reader = csv.reader(f, delimiter = "\t")
    blue_list = []
    red_list = []
    for row in reader:
        blue_list.append(int(row[0]))
        red_list.append(int(row[1]))
    blue_list = np.array(blue_list)
    red_list = np.array(red_list)
    return blue_list, red_list
