import csv
import scipy
import numpy as np
from scipy import stats
from binary_reader import parse_bin
from PAX_sampler_monomer import data_counter_blue, poisson_broad_sum2, poisson_broad
from scipy.special import gamma
from PAX_sampler_monomer import poisson_pmf
import matplotlib.pyplot as plt
import cPickle
import array

def MCS_process(filepath):
    # data = []
    data = array.array("i")
    f = open(filepath)
    f.seek(256)
    data.fromfile(f, 10000)  
    return data


def file_reader(file1):
    f = open(file1, "r")
    data = cPickle.load(f)
    data = np.array(data)
    print data
    f.close()
    return data

def poisson_broad_sum_distribution(value, lam1, R1, lam2, R2, maxv = 300):
    signal = np.arange(value + 1)
    p1 = poisson_broad(value - signal, lam1, R1)
    p2 = poisson_broad(signal, lam2, R2)
    ps = p1 * p2
    ps = ps / np.sum(ps)
    ps = np.hstack([ps, np.zeros(maxv - len(ps))])
    return ps


def read_data(filename, filepath, file_start, file_end):
    blue = []
    red = []
    data_tuples = []
    for i in range(file_start, file_end):
        name = "%s%04d.dat" %(filename, i)
        data_tuples = parse_bin(filepath, name, data_tuples)
    for j in data_tuples:
        blue.append(j[0])
        red.append(j[1])
    return blue, red

def gamma_poisson_pmf(x, lam_DB, R):
    c1 = lam_DB + R + 1
    num = gamma(c1 + 1)
    denom = gamma(c1 - lam_DB +1) * gamma(R)
    frac = float(num) / denom
    p1 = (1 - x)**R_blue
    p2 = x**lam_DB
    tot = frac*p1*p2
    return tot

def prot_calc(x, lam, factor):  
    prob = poisson_pmf(x, lam*factor)
    return prob    

def prob_calc(data, lam_NB, lam_DB, lam_prot, gamma, R_blue, r_noise):
    prob_list = []
    p_prot0, p_prot1, p_prot2, p_prot3, p_prot4, p_prot5, p_prot6, p_prot7, p_prot8 = poisson_pmf(0, lam_prot), poisson_pmf(1, lam_prot), poisson_pmf(2, lam_prot), poisson_pmf(3, lam_prot), poisson_pmf(4, lam_prot) , poisson_pmf(5, lam_prot), poisson_pmf(6, lam_prot), poisson_pmf(7, lam_prot), poisson_pmf(8, lam_prot) 

    p_noise = p_prot0 * poisson_pmf(data, lam_NB)
    p_dye_1 = p_prot1 * poisson_broad_sum2(data, lam_NB, r_noise, lam_DB, R_blue)
    p_dye_2 = p_prot2 * poisson_broad_sum2(data, lam_NB, r_noise, 2*lam_DB, R_blue)
    p_dye_3 = p_prot3 * poisson_broad_sum2(data, lam_NB, r_noise, 3*lam_DB, R_blue)
    p_dye_4 = p_prot4 * poisson_broad_sum2(data, lam_NB, r_noise, 4*lam_DB, R_blue)
    p_dye_5 = p_prot5 * poisson_broad_sum2(data, lam_NB, r_noise, 5*lam_DB, R_blue)
    p_dye_6 = p_prot6 * poisson_broad_sum2(data, lam_NB, r_noise, 6*lam_DB, R_blue)
    p_dye_7 = p_prot7 * poisson_broad_sum2(data, lam_NB, r_noise, 7*lam_DB, R_blue)
    p_dye_8 = p_prot8 * poisson_broad_sum2(data, lam_NB, r_noise, 8*lam_DB, R_blue)

    p_tot = p_noise + p_dye_1 + p_dye_2 + p_dye_3 + p_dye_4 + p_dye_5 + p_dye_6 + p_dye_7 + p_dye_8
    p0 = p_noise / p_tot
    p1 = p_dye_1 / p_tot
    p2 = p_dye_2 / p_tot
    p3 = p_dye_3 / p_tot
    p4 = p_dye_4 / p_tot
    p5 = p_dye_5 / p_tot
    p6 = p_dye_6 / p_tot
    p7 = p_dye_7 / p_tot
    p8 = p_dye_8 / p_tot
    
    prob = [p0, p1, p2, p3, p4, p5, p6, p7, p8]
    # prob_list.append((i, prob))

    return prob

#def buckets(prob_list, n):
#    noise = np.random.multinomial(n, prob_list)
#    monomer = np.random.multinomial(n, prob_list[1][1])
#    return noise, monomer
           


def main():

    #try:
    #    for i in range(180):
    #        data_blue = []
    #        filepath = "/media/TOSHIBA EXT/repos/size-infer/test_data/donor only/Dye/0"
    #        blue_file = "DNA_d_a_"
    #        data_blue += MCS_process("%s/%s%04d.mcs" % (filepath, blue_file, i))
    #except:
    #    raise

    blue, _ = read_data("asyn", "/media/TOSHIBA EXT/repos/fret-inference/test_data/monomers/t0", 0, 1)
    data_blue = [db for db in blue if db < 200]
    #data_blue = file_reader("/media/TOSHIBA EXT/repos/fret-inference/test_data/monomers/blue_channel_8.txt")
    blue_data, counter = data_counter_blue(data_blue)
    lam_NB = 1.275 # 0.2836044734 #
    lam_DB = 1.568 #8.1993934362 #
    lam_prot = 1.453 #0.0549645083 #
    R_blue = 0.137
    r_noise = 50
    #       
      

    dist_list = []
    #monomer = []
    #dimer = []
    #trimer = []
    #noise = []
    fracs = [[],[],[],[],[],[],[],[],[]]
    #sum_totals = []
    clean_monomer = []
    
    
    prob_list = prob_calc(blue_data, lam_NB, lam_DB, lam_prot, 1.0, R_blue, r_noise)

    
    for j in range(10):
        Data_photons = np.zeros(300)
        noise_photons = np.zeros(300)
        dimer_photons = np.zeros(300)
        other_photons = np.zeros(300)

        D_noise = poisson_pmf(np.arange(200), lam_NB)    
        f = open("/media/TOSHIBA EXT/repos/size-infer/test_data/donor only/Dye/0/badly_sizing_data_%s.csv" %(j), "w")
        for i,(v,n)  in enumerate(zip(blue_data, counter)):
            D = [pl[i] for pl in prob_list]
            dist = np.random.multinomial(n, D)
            print v, D
            ## Distribution, of denoised particles
            Dphotons = poisson_broad_sum_distribution(v, lam_NB, r_noise, lam_DB, R_blue)

            Data_photons += np.random.multinomial(dist[1], Dphotons)
            D_N = np.hstack([D_noise[0:i+1] / np.sum(D_noise[0:i+1]), np.zeros(300 - (i+1))])
            noise_photons += np.random.multinomial(dist[0], D_N) 

            Ddimers = poisson_broad_sum_distribution(v, lam_NB, r_noise, 2*lam_DB, R_blue)
            dimer_photons += np.random.multinomial(dist[2], Ddimers)
                
            for clump in range(3,9):
                Dclump = poisson_broad_sum_distribution(v, lam_NB, r_noise, clump*lam_DB, R_blue)
                other_photons += np.random.multinomial(dist[clump], Dclump)


            dist_list.append(dist)
            print v, dist
     
        # Distribution of data photos of single monomers
        print Data_photons

        print "means"
        print np.sum((Data_photons * np.arange(300)))/ np.sum(Data_photons)
        print np.sum((dimer_photons * np.arange(300)))/ np.sum(dimer_photons)

        for i in range(300):
            f.write("%s,%s\n" %(i, Data_photons[i]))
        f.close()
    
    
    plt.plot(noise_photons, color = "red", label = "noise")
    plt.plot(Data_photons, color = "blue", label = "monomers")
    plt.plot(dimer_photons, color = "green", label = "dimers")
    plt.plot(other_photons, color = "k", label = "big stuff")
    plt.xlabel("Photons")
    plt.ylabel("Frequency")
    plt.legend()
    plt.xlim((0,50))
    plt.ylim((0, 10000))
    #plt.yscale("log")
    plt.savefig("/media/TOSHIBA EXT/repos/size-infer/test_data/donor only/Dye/0/badly_sized_data_100.pdf")
    plt.show()
               
        
if __name__ == "__main__":
    main()
