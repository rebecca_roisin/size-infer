import numpy as np
from scipy.special import gamma, gammaln
from math import exp, e, log
import random

import numpy.random as npr

import scipy.stats

import matplotlib.pyplot as plt

def const(k0,k1, r, E, g, m):
    return ((1-E)**k0 * (g*E)**k1) / ((m/r)**r * gamma(r)*gamma(k0+1)*gamma(k1+1))

def target(k0,k1, r, E, g, m):
    return const(k0,k1, r, E, g, m)*(1-E+g*E+r/m)**(-(k0+k1+r)) * gamma(k0+k1+r)

def intTarget(k0,k1, r, E, g, m, x):
    return (x**(k0+k1+r - 1)) * e**(-(1-E+g*E+r/m)*x) 
    
def naive(k0,k1, r, E, g, m):
    s = 0.0
    samples = 100
    for i in range(samples):
        x = random.randint(0, 40)
        prob = 1.0 / 40
        s += intTarget(k0,k1, r, E, g, m,  x) * (1.0 / samples) * (1.0 / prob)
    return const(k0,k1, r, E, g, m) * s

def montecarlo(k0, k1, r, E, g, m):
    s = 0.0
    samples = 100
    for i in range(samples):
        rr = scipy.stats.gamma.rvs(r, scale=m/r)
        prob = scipy.stats.gamma.pdf(rr, r, scale=m/r)
        # r = random.randint(0, 40)
        s += intTarget(k0,k1, r, E, g, m, rr) * (1.0 / samples) * (1.0 / prob)
    return const(k0,k1, r, E, g, m) * s

def stripTarget(k0,k1, lam_NB, lam_NR, r, E, g, m, x):
    # return (x**(k0+k1)) * (e**(-x*(1-E+g*E)))
    newx0 = x*(1-E) + lam_NB 
    p0 = ((newx0)**k0) * (1.0/gamma(k0+1)) * (e**(-newx0))
    newx1 = x*E*g + lam_NR 
    p1 = ((newx1)**k1) * (1.0/gamma(k1+1)) * (e**(-newx1))

    # Add Gamma probability
    gg = (1 / ( gamma(r) * ( (m/r) ** r ) )) * (x**(r-1)) * (e**(-x/(m/r)))
    return p0*p1*gg 

def plotTarget(k0,k1, lam_NB, lam_NR, r, E, g, m):
    x = np.arange(0,50, 0.25)

    # return (x**(k0+k1)) * (e**(-x*(1-E+g*E)))
    newx0 = x*(1-E) + lam_NB 
    p0 = ((newx0)**k0) * (1.0/gamma(k0+1)) * (e**(-newx0))
    newx1 = x*E*g + lam_NR 
    p1 = ((newx1)**k1) * (1.0/gamma(k1+1)) * (e**(-newx1))

    # Add Gamma probability
    gg = (1 / ( gamma(r) * ( (m/r) ** r ) )) * (x**(r-1)) * (e**(-x/(m/r)))

    tot = p0*p1*gg
    plt.plot(x, tot/sum(tot))
    plt.plot(x, gg/sum(gg), "r")
    plt.plot(x, p0/sum(p0), "r")
    plt.plot(x, p1/sum(p1), "r")
    plt.show()


def stripTargetPoisson(k0, lam_NB, x):
    # return (x**(k0+k1)) * (e**(-x*(1-E+g*E)))
    newx0 = x + lam_NB 
    p0 = ((newx0)**k0) * (1.0/gamma(k0+1)) * (e**(-newx0))
    return p0 

# Your actual integral is:
# \integral_0^\inf Poisson_{(1-E)\lamda + \lambda_{NB}}(\lambda) *
# Poisson_{\gamma*E*\lamda + \lambda_{NR}}(\lambda) * Gamma(r, m/r)
# (\lambda) d \lambda
# So if you sample points from a Gamm(r, m/r) than you can simply sum:
# Poisson_{(1-E)\lamda + \lambda_{NB}}(\lambda) *
# Poisson_{\gamma*E*\lamda + \lambda_{NR}}(\lambda)

def stripmontecarlo(k0, k1, lam_NB, lam_NR, r, E, g, m, samples = 100):
    #rr = npr.gamma(r, m/r, size=samples) # scipy.stats.gamma.rvs(r, scale=r/(m))
    #prob = scipy.stats.gamma.pdf(rr, r, scale=m/r)
    rr = np.arange(0, 50, 50.0 / samples)
    samples2 = samples / 50  

    s = sum(stripTarget(k0, k1, lam_NB, lam_NR, r, E, g, m, rr)) * (1.0 / samples2) 
    return s

def stripmontecarloPoisson(k0, lam_NB, r, m, samples = 100):
    rr = npr.gamma(r, m/r, size=samples) # scipy.stats.gamma.rvs(r, scale=r/(m))
    
    s = sum(stripTargetPoisson(k0, lam_NB, rr)) * (1.0 / samples) 
    return s

def naivemanymc(k0L, k1L, lam_NB, lam_NR, r, E, g, m, samples = 100):
    return np.array([stripmontecarlo(k0, k1, lam_NB, lam_NR, r, E, g, m, samples) for k0,k1 in zip(k0L,k1L)])

def cachedmanymc(k0L, k1L, lam_NB, lam_NR, r, E, g, m, prints, samples = 100, allb=None, allr=None, LIMIT = 100.0):
    # what goes in..
    # data_blue, data_red, lam_NB, lam_NR, R_blue, E, gamma_ins, lamDB, 0, samples = 100

    weights = (LIMIT / samples)    
    x = np.arange(0.0, LIMIT, weights)    

    newx0 = x*(1-E) + lam_NB 
    c0 = (e**(-newx0))
    newx1 = x*E*g + lam_NR
    c1 = (e**(-newx1))

    gg = (1 / ( gamma(r) * ( (m/r) ** r ) )) * (x**(r-1)) * (e**(-(x*r)/(m)))

    p0s = [None] * 300
    p1s = [None] * 300

    if allb == None:
        allb = set(k0L)
    if allr == None:
        allr = set(k1L)

    #print "allb = ", allb
    #print "allr = ", allr

    for k0 in allb:
        p0s[k0] = e**(k0*np.log(newx0)-gammaln(k0+1)) 

    for k1 in allr:
        p1s[k1] = e**(k1*np.log(newx1)-gammaln(k1+1))

    s1 = []
    for (k0, k1) in zip(k0L, k1L):
        s1.append(c0*c1*gg*p0s[k0]*p1s[k1])    

    s = [np.sum(c0*c1*gg*p0s[k0]*p1s[k1]) * weights for (k0, k1) in zip(k0L, k1L)] 
    s = np.array(s)

    return s #, s1
    

if __name__=="__main__":
    print "Real: ", target(3,6, 2.0, 0.7, 1, 5)
    print "Naive: ", naive(3,6, 2.0, 0.7, 1, 5)
    print "Monte Carlo: ", montecarlo(3, 6, 2.0, 0.7, 1, 5)
    print "Strip Monte Carlo: ", stripmontecarlo(3, 6, 0.0, 0.0, 2.0, 0.7, 1, 5)

    import time
    a = np.arange(0,30,1)
    b = []
    r = []
    for i in a:
        for j in a:
          b += [i]
          r += [j]  

    t1 = time.time()
    ret = naivemanymc(b, r, 0.5, 1.5, 2.0, 0.7, 1, 10)
    t2 = time.time()
    print "Naive Took %s ms (S=%s)" % ((t2-t1)*1000, sum(ret))

    allb = set(a)
    allr = set(a)

    t1 = time.time()
    ret2 = cachedmanymc(b, r, 0.5, 1.5, 2.0, 0.7, 1, 10, 100)
    t2 = time.time()
    print "Clever Took %s ms (S=%s)" % ((t2-t1)*1000, sum(ret2))


    # plotTarget(12, 6, 0.5, 1.5, 2.0, 0.7, 1, 10)
